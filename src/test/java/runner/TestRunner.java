package runner;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\features",glue = {"com.yalla.pages","hooks"}, monochrome = true, tags ="@smoke or @reg" /*, dryRun=true, 
snippets =SnippetType.CAMELCASE*/)

public class TestRunner {

}
