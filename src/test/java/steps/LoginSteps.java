/*package steps;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class LoginSteps {
	
	ChromeDriver driver;
	
	@Given("open the browser")
	public void openTheBrowser() {
		System.setProperty("webdriver.chrome.driver",
				"./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}

	@Given("maximize the browser")
	public void maximizeTheBrowser() {
		
		driver.manage().window().maximize();
		
	}

	@Given("load the url as (.*)")
	public void loadTheUrlAsHttpLeaftapsComOpentaps(String url) {
		driver.get(url);
	}

	@Given("enter the username as (.*)")
	public void enterTheUsernameAsDemoSalesManager(String uName) {
	    driver.findElementById("username").sendKeys(uName);
	}

	@Given("enter the password as (.*)")
	public void enterThePasswordAsCrmsfa(String pwd) {
		  driver.findElementById("password").sendKeys(pwd);
	}

	@When("click on login button")
	public void clickOnLoginButton() {
	    driver.findElementByClassName("decorativeSubmit").click();
	}

	@Then("verify login success")
	public void verifyLoginSuccess() {
	    System.out.println("Login Success");
	}
	
	@Given("click on crmsfa link")
	public void clickOnCrmsfaLink() {
	   driver.findElementByLinkText("CRM/SFA").click();
	}

	@Given("click on Leads")
	public void clickOnLeads() {
	    driver.findElementByLinkText("Leads").click();
	}

	@Given("click on create lead")
	public void clickOnCreateLead() {
	    driver.findElementByLinkText("Create Lead").click();
	}

	@Given("enter company name as (.*)")
	public void enterCompanyName(String cName) {
	    driver.findElementById("createLeadForm_companyName").sendKeys(cName);
	}

	@Given("enter first name as (.*)")
	public void enterFirstName(String fName) {
		driver.findElementById("createLeadForm_firstName").sendKeys(fName);
	}

	@Given("enter last name as (.*)")
	public void enterLastName(String lName) {
		driver.findElementById("createLeadForm_lastName").sendKeys(lName);
	}

	@When("click submit")
	public void clickSubmit() {
	    driver.findElementByName("submitButton").click();
	}

	@Then("verify created lead")
	public void verifyCreatedLead() {
	 System.out.println("Lead created successfully");
	}


}
*/