package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC003_EditLead extends Annotations {
	
	@BeforeTest(groups = "smoke")
	public void setData() {
		testcaseName = "TC003_EditLead";
		testcaseDec = "Edit a Lead";
		author = "Bharathi";
		category = "smoke";
		excelFileName = "TC003";
	}
	
	@Test(groups ="smoke", dataProvider="fetchData")
	public void editLead(String uName, String pwd, String phoneNum,String cName) {
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd)
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickFindLeads()
		.clickPhone()
		.enterPhoneNum(phoneNum)
		.clickFindLeads()
		.clickFirstResult()
		.clickEdit()
		.updateCompanyName(cName)
		.clickUpdate()
		.clickLogout();
		
	}

}
