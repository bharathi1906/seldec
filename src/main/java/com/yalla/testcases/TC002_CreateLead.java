package com.yalla.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.yalla.pages.HomePage;
import com.yalla.pages.LoginPage;
import com.yalla.testng.api.base.Annotations;

public class TC002_CreateLead extends Annotations {
	
	@BeforeTest(groups = "smoke")
	public void setData() {
		testcaseName = "TC002_CreateLead";
		testcaseDec = "Create a new Lead";
		author = "Bharathi";
		category = "smoke";
		excelFileName = "TC002";
	}
	
	@Test(groups = "smoke", dataProvider= "fetchData")
	public void createLead(String uName,String pwd,String cName,String fName,String lName) {
		
		new LoginPage()
		.enterUserName(uName)
		.enterPassWord(pwd) 
		.clickLogin()
		.clickCRMSFA()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName(cName)
		.enterFirstName(fName)
		.enterLastName(lName)
		.clickCreateLead()
		.clickLogout();
	}

}
