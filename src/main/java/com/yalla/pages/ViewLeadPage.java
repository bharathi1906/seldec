package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.Then;

public class ViewLeadPage extends Annotations {
	
	public ViewLeadPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT, using = "Create Lead") WebElement eleCreateLead;
	public CreateLeadPage clickCreateLead() {
		click(eleCreateLead);
		return new CreateLeadPage();
	}
	
	@FindBy(how = How.LINK_TEXT, using ="Edit") WebElement eleEdit;
	public OpenTapsCRMPage clickEdit() {
		click(eleEdit);
		return new OpenTapsCRMPage();
	}
	
	
	@FindBy(how = How.LINK_TEXT, using ="Logout") WebElement eleLogout;
	public void clickLogout() {
	click(eleLogout);	
	}
	
	@Then("verify created lead")
	public ViewLeadPage createLeadSuccess() {
		System.out.println("Lead created successfully");
		return this;
	}

}
