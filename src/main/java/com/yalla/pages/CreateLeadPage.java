package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;

public class CreateLeadPage extends Annotations {
	
	public CreateLeadPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using ="createLeadForm_companyName") WebElement eleCompanyName;
	@And("enter company name as (.*)")
	public CreateLeadPage enterCompanyName(String data) {
		clearAndType(eleCompanyName, data);
		return this;
	}
	
	@FindBy(how = How.ID, using ="createLeadForm_firstName") WebElement eleFirstName;
	@And("enter first name as (.*)")
	public CreateLeadPage enterFirstName(String data) {
		clearAndType(eleFirstName, data);
		return this;
	}
	
	@FindBy(how = How.ID, using ="createLeadForm_lastName") WebElement eleLastName;
	@And("enter last name as (.*)")
	public CreateLeadPage enterLastName(String data) {
		clearAndType(eleLastName, data);
		return this;
	}
	
	@FindBy(how = How.NAME, using ="submitButton") WebElement eleSubmit;
	@When("click submit")
	public ViewLeadPage clickCreateLead() {
		click(eleSubmit);
		return new ViewLeadPage();
	}

}
