package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class OpenTapsCRMPage extends Annotations {

	public OpenTapsCRMPage(){
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.ID, using ="updateLeadForm_companyName") WebElement eleCompName;
	public OpenTapsCRMPage updateCompanyName(String data) {
		clearAndType(eleCompName, data);
		return this;
	}
	
	@FindBy(how= How.NAME, using ="submitButton") WebElement eleUpdate;
	public ViewLeadPage clickUpdate() {
		click(eleUpdate);
		return new ViewLeadPage();
	}
}
