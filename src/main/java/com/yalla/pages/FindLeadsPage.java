package com.yalla.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.yalla.testng.api.base.Annotations;

public class FindLeadsPage extends Annotations {
	
	public FindLeadsPage(){
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH,using ="//span[text()='Phone']") WebElement elePhone;
	public FindLeadsPage clickPhone() {
		click(elePhone);
		return this;
	}
	
	@FindBy(how = How.NAME,using ="phoneNumber") WebElement elePhoneNum;
	public FindLeadsPage enterPhoneNum(String data) {
		clearAndType(elePhoneNum, data);
		return this;
	}
	
	@FindBy(how = How.XPATH,using ="//button[text()='Find Leads']") WebElement eleFindLeads;
	public FindLeadsPage clickFindLeads() {
		click(eleFindLeads);
		return this;
	}
	

	@FindBy(how = How.XPATH, using ="//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a") WebElement eleFirstResult;
	public ViewLeadPage clickFirstResult() {
		click(eleFirstResult);
		return new ViewLeadPage();
	}
}
