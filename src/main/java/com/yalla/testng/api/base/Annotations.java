package com.yalla.testng.api.base;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;

import com.yalla.selenium.api.base.SeleniumBase;

import utils.DataLibrary;

public class Annotations extends SeleniumBase {

	@DataProvider(name = "fetchData")
	public Object[][] fetchData() throws IOException {
		return DataLibrary.readExcelData(excelFileName);
	}	

	@Parameters("url")
	@BeforeMethod(groups = "all")
	public void beforeMethod(String url) {
		startApp("chrome", url);

	}

	@AfterMethod(groups = "all")
	public void afterMethod() {
		close();
	}

}
